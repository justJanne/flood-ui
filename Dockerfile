FROM node:alpine AS builder
RUN apk add --no-cache git python3 build-base
RUN git clone https://github.com/Flood-UI/flood/ /flood-ui
WORKDIR /flood-ui
RUN npm install
RUN cp config.docker.js config.js
RUN npm run build
RUN npm prune --production

FROM node:alpine
RUN apk --no-cache add mediainfo
COPY --from=builder /flood-ui /flood-ui

WORKDIR /flood-ui
EXPOSE 3000
VOLUME ["/data"]

ENTRYPOINT ["npm", "start"]
